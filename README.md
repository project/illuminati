# Illuminati

Illuminati is a material theme based on Omega 4 and Materialize css.

## Installation

In order to install Illuminati, simply place it in your sites theme folder
(normally located at sites/all/themes). It uses Omega 4.x as a base theme.
So download and enable Omega theme as well.
